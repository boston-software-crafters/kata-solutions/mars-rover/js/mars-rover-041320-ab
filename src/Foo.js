class Foo {
  static getName() {
    return 'Foo!';
  }

  fibonacci = (n) =>
    n <= 1 ? 1 : this.fibonacci(n - 1) + this.fibonacci(n - 2);
}

export default Foo;
